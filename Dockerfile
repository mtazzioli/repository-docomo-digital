FROM openjdk:8-jdk-alpine
MAINTAINER marcelo.h.tazzioli@gmail.com
COPY target/docomo-digital-1.0.0-SNAPSHOT.jar docomo-digital.jar
ENTRYPOINT ["java","-jar","/docomo-digital.jar"]