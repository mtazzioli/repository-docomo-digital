package com.exercise.dto.base;

import com.exercise.validation.Delete;
import com.exercise.validation.New;
import com.exercise.validation.Update;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BaseDto {

    @Null(groups = { New.class }, message = "El id debe ser nulo al crear un objeto")
    @NotNull(groups = { Update.class, Delete.class }, message = "El id NO puede ser nulo al actualizar/eliminar un objeto")
    public Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

}
