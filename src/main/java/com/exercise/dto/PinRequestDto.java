package com.exercise.dto;

import com.exercise.dto.base.BaseDto;
import com.exercise.validation.New;
import com.exercise.validation.Verify;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class PinRequestDto extends BaseDto {

    @NotNull(groups = { New.class, Verify.class }, message = "phoneNumber is a mandatory field")
    @Pattern(groups = { New.class, Verify.class }, regexp = "[0-9]{12}", message = "phoneNumber invalid format")
    private String phoneNumber;

    @NotNull(groups = { Verify.class }, message = "pin is a mandatory field")
    @Pattern(groups = { Verify.class }, regexp = "[0-9]{6}", message = "pin invalid format")
    private String pinCode;

}
