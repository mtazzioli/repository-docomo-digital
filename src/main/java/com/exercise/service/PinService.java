package com.exercise.service;

import com.exercise.dto.PinRequestDto;

import java.security.NoSuchAlgorithmException;

public interface PinService {

    PinRequestDto generate(PinRequestDto pinRequestDto) throws NoSuchAlgorithmException;

    void verify(PinRequestDto pinRequestDto) throws Exception;
}
