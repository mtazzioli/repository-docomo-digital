package com.exercise.service.impl;

import com.exercise.constants.Constants;
import com.exercise.dto.PinRequestDto;
import com.exercise.exception.ValidationException;
import com.exercise.model.Pin;
import com.exercise.repository.PinRepository;
import com.exercise.service.PinService;
import com.exercise.validation.PinValidationService;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.apache.commons.math3.random.RandomDataGenerator;
import org.springframework.stereotype.Service;

import java.security.NoSuchAlgorithmException;
import java.util.Date;

@Service
public class PinServiceImpl implements PinService {

    private final PinRepository pinRepository;

    private final PinValidationService pinValidationService;

    private final ObjectMapper objectMapper;

    public PinServiceImpl(PinRepository pinRepository,
                          ObjectMapper objectMapper,
                          PinValidationService pinValidationService) {
        this.pinRepository = pinRepository;
        this.objectMapper = objectMapper;
        this.pinValidationService = pinValidationService;
    }

    private PinRequestDto toDto(Pin entity) {
        return objectMapper.convertValue(entity, PinRequestDto.class);
    }

    @Override
    public PinRequestDto generate(PinRequestDto pinRequestDto) throws NoSuchAlgorithmException {
        RandomDataGenerator randomDataGenerator = new RandomDataGenerator();

        int pinCode = randomDataGenerator.nextInt(100000, 999999);

        Pin pin = Pin.builder()
                .phoneNumber(pinRequestDto.getPhoneNumber())
                .pinCode(String.valueOf(pinCode))
                .dateTime(new Date())
                .attempts(Constants.MAX_ATTEMPTS)
                .build();
        pinRepository.save(pin);

        return toDto(pin);
    }

    @Override
    public void verify(PinRequestDto pinRequestDto) throws Exception {
        Pin pin = pinRepository.findFirstByPhoneNumberOrderByDateTimeDesc(
                pinRequestDto.getPhoneNumber());

        pinValidationService.validate(pin);

        int leftAttempts = pin.getAttempts() - 1;
        pin.setAttempts(leftAttempts);
        pinRepository.save(pin);

        if (!pin.getPinCode().equals(pinRequestDto.getPinCode())) {
            throw new ValidationException(String.format("Incorrect pin code, leftAttempts: %s", leftAttempts));
        }
    }
}
