package com.exercise.controller;

import com.exercise.dto.PinRequestDto;
import com.exercise.exception.ValidationException;
import com.exercise.service.PinService;
import com.exercise.validation.New;
import com.exercise.validation.Verify;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("pin")
public class PinController {

    private final PinService pinService;

    public PinController(PinService pinService) {
        this.pinService = pinService;
    }

    @PostMapping(value = "generate")
    public ResponseEntity<Object> generate(@Validated(New.class) @RequestBody PinRequestDto pinRequestDto) {
        try {
            return new ResponseEntity<>(pinService.generate(pinRequestDto), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(value = "verify")
    public ResponseEntity<Object> verify(@Validated(Verify.class) @RequestBody PinRequestDto pinRequestDto) {
        try {
            pinService.verify(pinRequestDto);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (ValidationException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_ACCEPTABLE);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
