package com.exercise.validation;

import com.exercise.exception.ValidationException;
import com.exercise.model.Pin;

public interface PinValidationService {

    void validate(Pin pin) throws ValidationException;

}
