package com.exercise.validation.impl;

import com.exercise.constants.Constants;
import com.exercise.exception.ValidationException;
import com.exercise.model.Pin;
import com.exercise.util.DateUtils;
import com.exercise.validation.PinValidationService;

import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.concurrent.TimeUnit;

@Service
public class PinValidationServiceImpl implements PinValidationService {

    private final DateUtils dateUtils;

    public PinValidationServiceImpl(DateUtils dateUtils) {
        this.dateUtils = dateUtils;
    }

    @Override
    public void validate(Pin pin) throws ValidationException {
        try {
            Assert.notNull(pin, "Incorrect phoneNumber and pin code");
            Assert.isTrue(pin.getAttempts() > 0, "Max attempts reached, get a new pin code");

            long diff = (dateUtils.today()).getTime() - pin.getDateTime().getTime();
            long minutes = TimeUnit.MILLISECONDS.toMinutes(diff);
            Assert.isTrue(minutes < Constants.PIN_DURATION, "Pin expired");
        } catch (IllegalArgumentException ex) {
            String exceptionMessage = "Pin code validation failed: " + ex.getMessage();
            throw new ValidationException(exceptionMessage);
        }
    }
}
