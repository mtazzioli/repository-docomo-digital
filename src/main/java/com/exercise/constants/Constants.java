package com.exercise.constants;

/**
 * This class defines the constants that are used for general purposes.
 */
public class Constants {

    public static final int MAX_ATTEMPTS = 3;
    public static final int PIN_DURATION = 20;

    private Constants() {
        // Utility classes, which are collections of static members, are not meant to be instantiated.
    }

}
