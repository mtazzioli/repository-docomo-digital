package com.exercise.exception;

public class ValidationException extends Exception {

    private static final long serialVersionUID = 1L;

    public ValidationException(String msj) {
        super(msj);
    }

}
