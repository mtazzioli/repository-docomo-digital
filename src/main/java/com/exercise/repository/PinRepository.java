package com.exercise.repository;

import com.exercise.model.Pin;
import com.exercise.repository.base.BaseCrudRepository;

public interface PinRepository extends BaseCrudRepository<Pin> {

    Pin findFirstByPhoneNumberOrderByDateTimeDesc(String phoneNumber);
}
