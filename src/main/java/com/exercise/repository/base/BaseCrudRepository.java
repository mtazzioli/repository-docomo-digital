package com.exercise.repository.base;

import com.exercise.model.base.AbstractPersistentObject;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface BaseCrudRepository<E extends AbstractPersistentObject>
		extends JpaRepository<E, Long>, JpaSpecificationExecutor<E> {

	E findFirstById(Long id);
}
