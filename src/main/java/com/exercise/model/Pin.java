package com.exercise.model;

import com.exercise.model.base.AbstractPersistentObject;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Table(name = "pin")
public class Pin extends AbstractPersistentObject {

    private String phoneNumber;

    private String pinCode;

    private Date dateTime;

    private Integer attempts;

}
