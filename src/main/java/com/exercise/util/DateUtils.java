package com.exercise.util;

import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class DateUtils {

    private DateUtils() {
        // Utility classes, which are collections of static members, are not meant to be instantiated.
    }

    public java.util.Date today() {
        return new Date();
    }

}
