package com.exercise.service.impl;

import com.exercise.dto.PinRequestDto;
import com.exercise.exception.ValidationException;
import com.exercise.model.Pin;
import com.exercise.repository.PinRepository;
import com.exercise.service.PinService;
import com.exercise.validation.PinValidationService;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;

import java.security.NoSuchAlgorithmException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatExceptionOfType;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

class PinServiceImplTest {

    private PinService pinService;
    @Mock
    private PinRepository pinRepository;
    @Mock
    private ObjectMapper objectMapper;
    @Mock
    private PinValidationService pinValidationServices;
    @Captor
    private ArgumentCaptor<Pin> pinArgument;

    private PinRequestDto pinRequestDto;
    private Pin pin;
    private String phoneNumber;
    private String pinCode;
    private String generatedPin;

    @BeforeEach
    void setUp() {
        initMocks(this);
        pinService = new PinServiceImpl(pinRepository, objectMapper, pinValidationServices);

        phoneNumber = "393282172548";
        pinCode = "123456";
        generatedPin = "393282";
        pinRequestDto = PinRequestDto.builder().phoneNumber(phoneNumber).build();
        pin = Pin.builder().phoneNumber(phoneNumber).pinCode(pinCode).attempts(3).build();
    }

    @Test
    void shouldGeneratePinSuccessfully() throws NoSuchAlgorithmException {
        PinRequestDto returnPinRequestDto = PinRequestDto.builder()
                .phoneNumber(phoneNumber)
                .pinCode(generatedPin)
                .build();
        when(objectMapper.convertValue(any(Pin.class), any(Class.class))).thenReturn(returnPinRequestDto);

        PinRequestDto result = pinService.generate(pinRequestDto);

        assertThat(result.getPhoneNumber()).isEqualTo(phoneNumber);
        assertThat(result.getPinCode()).isEqualTo(generatedPin);
    }

    @Test
    void shouldSaveIntoDataBasePinGenerated() throws NoSuchAlgorithmException {
        PinRequestDto returnPinRequestDto = PinRequestDto.builder()
                .phoneNumber(phoneNumber)
                .pinCode(generatedPin)
                .build();
        when(objectMapper.convertValue(any(Pin.class), any(Class.class))).thenReturn(returnPinRequestDto);

        pinService.generate(pinRequestDto);

        verify(pinRepository).save(any(Pin.class));
    }

    @Test
    void shouldVerifyPinSuccessfully() throws Exception {
        when(pinRepository.findFirstByPhoneNumberOrderByDateTimeDesc(phoneNumber)).thenReturn(
                pin);
        doNothing().when(pinValidationServices).validate(pin);
        pinRequestDto.setPinCode(pinCode);

        pinService.verify(pinRequestDto);
    }

    @Test
    void shouldReduceAttemptWhenVerifyPinFail() throws Exception {
        when(pinRepository.findFirstByPhoneNumberOrderByDateTimeDesc(phoneNumber)).thenReturn(
                pin);
        doNothing().when(pinValidationServices).validate(pin);
        pinRequestDto.setPinCode(pinCode);

        pinService.verify(pinRequestDto);

        verify(pinRepository).save(pinArgument.capture());
        assertThat(pinArgument.getValue().getAttempts()).isEqualTo(2);
    }

    @Test
    void shouldThrowValidationExceptionWhenValidationFail() throws Exception {
        when(pinRepository.findFirstByPhoneNumberOrderByDateTimeDesc(phoneNumber)).thenReturn(
                pin);

        assertThatExceptionOfType(ValidationException.class).isThrownBy(
                        () -> pinService.verify(pinRequestDto))
                .withMessageContaining("Incorrect pin code, leftAttempts:");
    }
}