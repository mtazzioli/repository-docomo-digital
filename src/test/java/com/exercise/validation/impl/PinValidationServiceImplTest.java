package com.exercise.validation.impl;

import com.exercise.exception.ValidationException;
import com.exercise.model.Pin;
import com.exercise.util.DateUtils;
import com.exercise.validation.PinValidationService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import java.util.Date;

import static org.assertj.core.api.AssertionsForClassTypes.assertThatExceptionOfType;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

class PinValidationServiceImplTest {

    @Mock
    private DateUtils dateUtils;

    private PinValidationService pinValidationService;
    private Pin pin;

    @BeforeEach
    void setUp() {
        initMocks(this);
        pinValidationService = new PinValidationServiceImpl(dateUtils);

        pin = Pin.builder().attempts(1).build();

        Date time = new Date();
        when(dateUtils.today()).thenReturn(time);
        pin.setDateTime(org.apache.commons.lang3.time.DateUtils.addMinutes(time, -10));
    }

    @Test
    void shouldValidateSuccessfully() throws ValidationException {
        pinValidationService.validate(pin);
    }

    @Test
    void shouldThrowValidationExceptionWhenParameterIsNull() throws ValidationException {
        pin = null;

        assertThatExceptionOfType(ValidationException.class).isThrownBy(
                        () -> pinValidationService.validate(pin))
                .withMessageContaining("Incorrect phoneNumber and pin code");
    }

    @Test
    void shouldThrowValidationExceptionWhenAttemptsIsZero() throws ValidationException {
        pin.setAttempts(0);

        assertThatExceptionOfType(ValidationException.class).isThrownBy(
                        () -> pinValidationService.validate(pin))
                .withMessageContaining("Max attempts reached, get a new pin code");
    }

    @Test
    void shouldThrowValidationExceptionWhenPinExpired() throws ValidationException {
        Date time = new Date();
        when(dateUtils.today()).thenReturn(time);
        pin.setDateTime(org.apache.commons.lang3.time.DateUtils.addMinutes(time, -20));

        assertThatExceptionOfType(ValidationException.class).isThrownBy(
                        () -> pinValidationService.validate(pin))
                .withMessageContaining("Pin expired");
    }

}