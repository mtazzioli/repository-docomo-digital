package com.exercise.controller;

import com.exercise.dto.PinRequestDto;
import com.exercise.exception.ValidationException;
import com.exercise.service.PinService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.security.NoSuchAlgorithmException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

class PinControllerTest {

    @Mock
    private PinService pinService;

    private PinController pinController;
    private PinRequestDto pinRequestDto;

    @BeforeEach
    void setUp() {
        initMocks(this);
        pinController = new PinController(pinService);
        pinRequestDto = PinRequestDto.builder().build();
    }

    @Test
    void shouldReturnHttp200WhenPinGenerationIsSuccessful() throws NoSuchAlgorithmException {
        when(pinService.generate(pinRequestDto)).thenReturn(null);

        ResponseEntity<?> responseEntity = pinController.generate(pinRequestDto);

        assertThat(responseEntity.getHeaders()).isNotNull();
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    @Test
    void shouldReturnHttp500WhenPinGenerationFail() throws NoSuchAlgorithmException {
        doThrow(NoSuchAlgorithmException.class).when(pinService).generate(pinRequestDto);

        ResponseEntity<?> responseEntity = pinController.generate(pinRequestDto);

        assertThat(responseEntity.getHeaders()).isNotNull();
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @Test
    void shouldReturnHttp200WhenPinVerificationIsSuccessful() throws Exception {
        ResponseEntity<?> responseEntity = pinController.verify(pinRequestDto);

        verify(pinService).verify(pinRequestDto);
        assertThat(responseEntity.getHeaders()).isNotNull();
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    @Test
    void shouldReturnHttp406WhenPinVerificationValidationFail() throws Exception {
        doThrow(ValidationException.class).when(pinService).verify(pinRequestDto);

        ResponseEntity<?> responseEntity = pinController.verify(pinRequestDto);

        assertThat(responseEntity.getHeaders()).isNotNull();
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.NOT_ACCEPTABLE);
    }

    @Test
    void shouldReturnHttp500WhenPinVerificationFail() throws Exception {
        doThrow(Exception.class).when(pinService).verify(pinRequestDto);

        ResponseEntity<?> responseEntity = pinController.verify(pinRequestDto);

        assertThat(responseEntity.getHeaders()).isNotNull();
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR);
    }

}